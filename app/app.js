
angular.module('fleetio', ['ngRoute', 'mgcrea.ngStrap.navbar'])

.config(['$routeProvider',function($routeProvider) {
	$routeProvider
		.when('/dashboard', {
			templateUrl: 'views/dashboard.html'
		})
		.when('/vehicles', {
			templateUrl: 'views/vehicles.html'
		})
		.when('/services', {
			templateUrl: 'views/services.html'
		})
		.when('/fuel', {
			templateUrl: 'views/fuel.html'
		})
		.when('/servicereminder', {
			templateUrl: 'views/servicereminder.html'
		})
		.when('/renewalreminder', {
			templateUrl: 'views/renewalreminder.html'
		})
		.when('/vendors', {
			templateUrl: 'views/vendors.html'
		})
		.when('/bookings', {
			templateUrl: 'views/bookings.html'
		})
		.when('/reports', {
			templateUrl: 'views/reports.html'
		})
		.otherwise({
	        redirectTo: '/dashboard'
	    });
}])